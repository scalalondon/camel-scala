import org.mockito.{Mockito}
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfter, GivenWhenThen, FeatureSpec, Matchers}

import org.mockito.Mockito._

/**
  * Created by tony_murphy on 04/11/2015.
  */
class MyMocksSpec extends FeatureSpec with Matchers with GivenWhenThen with BeforeAndAfter with MockitoSugar {

  var myService: MyService = _

  var myController: MyController = _

  before {
    myService = mock[MyService]
    myController = new MyController(myService)
  }

  feature("a feature") {
    scenario("a scenario") {

      Mockito.doThrow(new RuntimeException).when(myService).sideEffect()
      Mockito.doNothing().when(myService).sideEffect()

      myController.showWebPage()
      myController.showWebPage()

      verify(myService, atLeastOnce()).sayHello("tony")
      verify(myService, atLeastOnce()).sideEffect()

    }
  }

}


class MyService {

  def sayHello(name: String) = {
    "hello " + name
  }

  def sideEffect(): Unit = {
    println("well i'm not doing much")
  }

}

class MyController(myService: MyService) {

  /**
    *
    */
  def showWebPage(): Unit = {
    myService.sideEffect()
    myService.sayHello("tony")
  }

}