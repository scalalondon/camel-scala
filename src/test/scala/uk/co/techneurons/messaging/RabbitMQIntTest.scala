package uk.co.techneurons.messaging

import com.rabbitmq.client.{ConnectionFactory, Connection, AMQP, Channel}
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.direct.{DirectEndpoint, DirectComponent}
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.{Endpoint, CamelContext}
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.test.junit4.{CamelTestSupport, CamelTestWatcher}
import org.scalatest._

object RabbitMQServer {

  val host = "192.168.99.100"
  val port = 5672
  val username = "guest"
  val password = "guest"
  val virtualHost = "/"
  val X_DEAD_LETTER_EXCHANGE = "x_dead_letter_exchange"
  val RABBITMQ_ROUTING_KEY = "rabbitmq.ROUTING_KEY"

  type RoutingKey = String
}

trait RabbitMQServer {

  import RabbitMQServer._

  private val factory = createFactory()

  private def createFactory(): ConnectionFactory = {

    import RabbitMQServer._

    val factory: ConnectionFactory = new ConnectionFactory()
    factory.setHost(host)
    factory.setPort(port)
    factory.setUsername(username)
    factory.setPassword(password)
    factory.setVirtualHost(virtualHost)
    factory

  }

  def createConnection: Connection = {
    factory.newConnection()
  }

  /**
    *
    * @param exchangeName
    * @param queueName
    * @param autoDelete
    * @param channel
    * @return routing key to send messages via the exchange to the queue
    */
  def createTopicExchange(exchangeName: String, queueName: String, autoDelete: Boolean, channel: Channel): RoutingKey = {

    val exchangeType: String = "topic"
    val exclusive: Boolean = false
    var routingKey: RoutingKey = queueName + ".route"
    val durable = autoDelete
    channel.exchangeDeclare(exchangeName, exchangeType, durable, autoDelete, null)

    import scala.collection.JavaConverters._
    import RabbitMQServer.X_DEAD_LETTER_EXCHANGE

//    val dealLetterRoutingKey: String = "some-routing-key"
    val arguments: Map[String, AnyRef] = Map(X_DEAD_LETTER_EXCHANGE -> (queueName + ".dlx"))
//      ("x-dead-letter-routing-key", dealLetterRoutingKey))

    channel.queueDeclare(queueName, durable, exclusive, autoDelete, arguments.asJava)
    channel.queueBind(queueName, exchangeName, routingKey)

    channel.exchangeDeclare(exchangeName + ".dlx", "topic", durable, autoDelete, null)
    channel.queueDeclare(queueName + ".d", durable, exclusive, autoDelete, null)
    channel.queueBind(queueName + ".d", exchangeName + ".dlx", routingKey)

    routingKey
  }

  def createExchange(f: (String,String,Boolean,Channel) => RoutingKey, exchangeName: String, queueName: String, autoDelete: Boolean, channel: Channel): RoutingKey = {
    f(exchangeName,queueName,autoDelete,channel)
  }


  def queueExists(queueName: String): Boolean = {
    false
  }

}

/**
  * Define RabbitMQ Connection.
  *
  * @param host
  * @param port
  * @param username
  * @param password
  * @param exchange
  * @param queue
  * @param routingKey
  * @param declare
  * @param autoDelete
  * @param autoAck
  */
case class CamelRabbitMQUriBuilder(host: String, port: Int, username: String, password: String,
                            exchange: String, queue: Option[String] = None, routingKey: Option[String] = None,
                            declare: Boolean = false, autoDelete: Boolean = true,
                            durable: Boolean = false,
                            autoAck: Boolean = false, exchangeType: String = "topic") {

  def build: String = {
    var uri: String = s"rabbitmq:${host}:${port}/${exchange}?username=${username}&password=${password}" +
      s"&durable=${durable}&autoAck=${autoAck}&autoDelete=${autoDelete}&exchangeType=${exchangeType}&declare=${declare}"
    if(queue.isDefined) uri = uri.concat(s"&queue=${queue.get}")
    if(routingKey.isDefined) uri = uri.concat(s"&routingKey=${routingKey.get}")
    uri
  }
}

/**
  *
  */
class RabbitMQIntTest extends org.scalatest.fixture.FeatureSpec
  with BeforeAndAfter with BeforeAndAfterEach with GivenWhenThen with Logging with RabbitMQServer {

  import RabbitMQServer._


  case class FixtureParam(connection: Connection, channel: Channel,
                          camelContext: CamelContext, routingkey: RoutingKey,
                          producerEndpointUri: String, consumerEndpointUri: String)

  def createCamelContext(exchange: String, queue: String, endpointUri: String, producerEndpointUri: String): CamelContext = {

    val consumerRmqUri = CamelRabbitMQUriBuilder(host, port, username, password, exchange, queue = Some(queue) ).build
    val producerRmqUri = CamelRabbitMQUriBuilder(host, port, username, password, exchange ).build

    val camelContext: CamelContext = new DefaultCamelContext
    val builder: RouteBuilder {def configure(): Unit} = new RouteBuilder() {
      override def configure(): Unit = {
        from(consumerRmqUri).to(endpointUri).routeId("Rabbit Consumer")
        from(producerEndpointUri).id("Direct:Start").startupOrder(1).to(producerRmqUri).id("RMQP").routeId("Rabbit Producer")
      }
    }
    camelContext.addRoutes(builder)
    logger.info(s"Consumer Camel Context ${camelContext} ${camelContext.getComponentNames}")
    camelContext
  }

  /**
   * <p>
   *   http://www.scalatest.org/user_guide/sharing_fixtures
   * </p>
   */
  def withFixture(test: OneArgTest) = {

    val connection = createConnection
    val channel = connection.createChannel

    val consumerEndpointUri: String = "mock:result"
    val producerEndpointUri: String = "direct:start"

    val autoDelete: Boolean = true
    val routingKey: RoutingKey = createExchange(createTopicExchange, exchangeName, queueName, autoDelete, channel)

    val camelContext: CamelContext = createCamelContext(exchangeName, queueName, consumerEndpointUri, producerEndpointUri)
    camelContext.start

    val theFixture = FixtureParam(connection, channel, camelContext, routingKey, producerEndpointUri, consumerEndpointUri)

    try {
      withFixture(test.toNoArgTest(theFixture))
    } finally {
      if(channel.isOpen) channel.close
      if(connection.isOpen) connection.close
      camelContext.stop
    }
  }

  private val exchangeName: String = "ex1"
  private val queueName: String = "q1"
  feature("Consume Message") {
    scenario("When a message is sent, it should be consumed") { f =>

      Given("RabbitMQ Exchange, Queue, DLX are Configured Properly")

      val produceTemplate = f.camelContext.createProducerTemplate

      val producerEndpoint: DirectEndpoint = f.camelContext.getEndpoint(f.producerEndpointUri, classOf[DirectEndpoint])
      val consumerEndpoint: MockEndpoint = f.camelContext.getEndpoint(f.consumerEndpointUri, classOf[MockEndpoint])

      consumerEndpoint.expectedMessageCount(1)
      consumerEndpoint.setResultWaitTime(5000)
      val expectedBody: String = "hello world"
      consumerEndpoint.expectedBodiesReceived(expectedBody)
      val properties: AMQP.BasicProperties.Builder = new AMQP.BasicProperties.Builder()
      f.channel.basicPublish(exchangeName, f.routingkey, properties.build(), expectedBody.getBytes("UTF-8"))

//      produceTemplate.setDefaultEndpointUri(f.producerEndpointUri)
//      produceTemplate.sendBodyAndHeader(expectedBody, RABBITMQ_ROUTING_KEY, f.routingkey)

      consumerEndpoint.assertIsSatisfied(5000)

    }

  }


}
