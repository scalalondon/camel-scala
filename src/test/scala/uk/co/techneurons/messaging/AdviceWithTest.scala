package uk.co.techneurons.messaging

import org.apache.camel.builder.{AdviceWithRouteBuilder, RouteBuilder}
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.{Endpoint, EndpointInject}
import org.apache.camel.test.junit4.CamelTestSupport
import org.scalatest.{BeforeAndAfterEach, FeatureSpecLike}
import scala.beans.BeanProperty

/**
 *
 */
class AdviceWithTest extends CamelTestSupport with FeatureSpecLike with BeforeAndAfterEach {

  @EndpointInject(uri ="rabbitmq:localhost:5672/ex1?username=guest&password=guest")
  @BeanProperty var rabbit: Endpoint = _

  @EndpointInject(uri = "mock:result")
  @BeanProperty var to: MockEndpoint = _

  override def beforeEach(): Unit = {
    super.setUp()
  }

  override def afterEach(): Unit = {
    super.tearDown()
  }

  override def createRouteBuilder(): RouteBuilder = {
    new RouteBuilder() {
      override def configure(): Unit = {
        from(rabbit).to(to).routeId("my-route")
      }
    }
  }

  feature("Consume Message") {

    scenario("advice builder...") {
      val routeBuilder: AdviceWithRouteBuilder = new AdviceWithRouteBuilder {
        override def configure(): Unit = {
          interceptSendToEndpoint(rabbit.getEndpointUri)
            .skipSendToOriginalEndpoint()
            .to("mock:catchend")
        }
      }
      context.getRouteDefinition("my-route").adviceWith(context, routeBuilder)
    }
  }

}

