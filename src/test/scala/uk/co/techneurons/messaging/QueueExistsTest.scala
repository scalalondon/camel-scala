package uk.co.techneurons.messaging

import com.rabbitmq.client.{Channel, Connection, AMQP}
import org.apache.camel.CamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.direct.DirectEndpoint
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.impl.DefaultCamelContext
import org.scalatest.{Matchers, GivenWhenThen, BeforeAndAfterEach, BeforeAndAfter}

class QueueExistsTest extends org.scalatest.fixture.FeatureSpec with BeforeAndAfter with BeforeAndAfterEach
  with GivenWhenThen with Matchers with Logging with RabbitMQServer {

  case class CamelRabbitMQUriBuilder(host: String, port: Int, username: String, password: String,
                                     exchange: String, queue: Option[String] = None, routingKey: Option[String] = None) {

    def build: String = {
      var uri: String = s"rabbitmq:${host}:${port}/${exchange}?username=${username}&password=${password}&declare=false"
      if(queue.isDefined) uri = uri.concat(s"&queue=${queue.get}")
      if(routingKey.isDefined) uri = uri.concat(s"&routingKey=${routingKey.get}")
      uri
    }
  }

  import RabbitMQServer._

  private val exchangeName: String = "advert.events"
  private val queueName: String = "finding.esapi.secondary.advertsToProcess1"

  val consumerRmqUri = CamelRabbitMQUriBuilder(host, port, username, password, exchange = "advert.events",
    queue = Some(queueName) ).build

  case class FixtureParam(connection: Connection, channel: Channel,
                          camelContext: CamelContext,
                          producerEndpointUri: String, consumerEndpointUri: String)

  def createCamelContext(exchange: String, queue: String, endpointUri: String): CamelContext = {

    val camelContext: CamelContext = new DefaultCamelContext
    val builder: RouteBuilder {def configure(): Unit} = new RouteBuilder() {
      override def configure(): Unit = {
        from(consumerRmqUri).to(endpointUri).routeId("Rabbit Consumer")
      }
    }
    camelContext.addRoutes(builder)
    logger.info(s"Consumer Camel Context ${camelContext} ${camelContext.getComponentNames}")
    camelContext
  }

  /**
    * <p>
    *   http://www.scalatest.org/user_guide/sharing_fixtures
    * </p>
    */
  def withFixture(test: OneArgTest) = {

    val connection = createConnection
    val channel = connection.createChannel

    val producerEndpointUri: String = "direct:start"
    val consumerEndpointUri: String = "mock:result"

    val camelContext: CamelContext = createCamelContext(exchangeName, queueName, consumerEndpointUri)
    camelContext.start

    val theFixture = FixtureParam(connection, channel, camelContext, producerEndpointUri, consumerEndpointUri)

    try {
      withFixture(test.toNoArgTest(theFixture))
    } finally {
      if(channel.isOpen) channel.close
      if(connection.isOpen) connection.close
      camelContext.stop
    }
  }

  feature("Check Queue Availability") {
    scenario("queue exists") { f =>
      Given("RabbitMQ Exchange, Queue are Configured Properly")
      val producerEndpoint: DirectEndpoint = f.camelContext.getEndpoint(f.producerEndpointUri, classOf[DirectEndpoint])
      val consumerEndpoint: MockEndpoint = f.camelContext.getEndpoint(f.consumerEndpointUri, classOf[MockEndpoint])

      f.camelContext.hasEndpoint(consumerRmqUri) shouldBe false

    }

  }

}
