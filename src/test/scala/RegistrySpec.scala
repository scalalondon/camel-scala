import org.apache.camel.impl.JndiRegistry
import org.apache.camel.test.junit4.CamelTestSupport
import org.scalatest.{FlatSpecLike, Matchers, FlatSpec}

/**
 * Created by tonymurphy on 11/2/15.
 */
class RegistrySpec extends CamelTestSupport with FlatSpecLike with Matchers {

  behavior of "Registry"

  it should "" in {

    val jndiRegistry = super.createRegistry()
    jndiRegistry.bind("hello",classOf[String])

    println(s""+jndiRegistry.lookup("hello"))

  }

}
