package uk.co.techneurons.messaging

import java.io.InputStream
import javax.naming.{InitialContext, Context}

import org.apache.camel._
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.impl.{SimpleRegistry, JndiRegistry, DefaultCamelContext}
import org.apache.camel.model.dataformat.JsonLibrary

import java.util.{Hashtable => JHashtable}

import _root_.scala.beans.BeanProperty

/**
 * Sample RabbitMQ Consumer...
 */
object RabbitMQConsumer extends App {


//  def createRegistry(): JndiRegistry = {
//    val jndiRegistry = new JndiRegistry(createJndiContext())
//    jndiRegistry.bind("myBean", classOf[JsonBeanExample])
//    jndiRegistry
//  }
//
//  def createJndiContext(): Context = {
//    val properties = new java.util.Properties()
//    val in: InputStream = this.getClass().getClassLoader().getResourceAsStream("jndi.properties");
//    if(in != null) {
//      properties.load(in);
//    } else {
//      properties.put("java.naming.factory.initial", "org.apache.camel.util.jndi.CamelInitialContextFactory");
//    }
//
//    val hashtable: JHashtable[AnyRef, AnyRef] = new JHashtable(properties)
//    return new InitialContext(hashtable);
//  }

//  private val createRegistry1: JndiRegistry = createRegistry()
//  val camelContext = new DefaultCamelContext(createRegistry1)
//
//  println(s"${createRegistry1.}")
//
//  val jndiRegistry = camelContext.getRegistry()
//
//  println(s"${camelContext.getRegistry()}")
//
////  jndiRegistry.bind("myBean",classOf[JsonBeanExample])
//

  val registry: SimpleRegistry  = new SimpleRegistry()
  registry.put("myBean", new JsonBeanExample())
  val camelContext = new DefaultCamelContext(registry)

  println(s"BEAN========= ${camelContext.getRegistry.lookupByName("myBean")}")

  val startEndpoint = "rabbitmq:localhost:5672/advert?queue=es_index&exchangeType=topic&autoDelete=false&declare=false&autoAck=false"

  val consumer = camelContext.createConsumerTemplate

  val routeBuilder = new RouteBuilder() {
    override def configure(): Unit = {

      errorHandler(defaultErrorHandler()
        .useOriginalMessage()
        .maximumRedeliveries(3)
        .redeliveryDelay(1000)
        .backOffMultiplier(4)
        .allowRedeliveryWhileStopping(false)
        .retryAttemptedLogLevel(LoggingLevel.WARN)
        .asyncDelayedRedelivery()
      .maximumRedeliveryDelay(15000)
        .useExponentialBackOff()
      )

//      onException(classOf[XPathException], classOf[TransformerException]).to("log:xml?level=WARN")
//      onException(classOf[ValidationException]).log("Validation error, Forget about ${body}").continued(true)
//      onException(classOf[InvalidPayloadException], classOf[NoTypeConversionAvailableException]).retryAttemptedLogLevel(LoggingLevel.WARN).log("Validation error, Forget about ${body}").logHandled(true)


//      from(startEndpoint)
//        .transform(body().convertToString()).id("Byte[] -> String")
//        .marshal().json(JsonLibrary.Jackson, classOf[Payload])
//        .bean(classOf[JsonBeanExample],"process").id("Payload Processor")

      from(startEndpoint).routeId("Index Advert")
        .unmarshal.json(JsonLibrary.Jackson, classOf[Payload]).id("Json Transform")
        .to("bean:myBean?method=process").id("Process Payload")

    }
  }


  camelContext.addRoutes(routeBuilder)


  camelContext.start

  Thread.sleep(20000)

  camelContext.stop

}

class Payload {
  @BeanProperty var id: Long = _


  override def toString = s"Payload($id)"
}

class JsonBeanExample() {

  def process(payload: Payload): Unit = {
    println(s"recieved ${payload}")
    if(payload.id == 1) throw new RuntimeException("ID==1 so, throw an exception")
  }

}


