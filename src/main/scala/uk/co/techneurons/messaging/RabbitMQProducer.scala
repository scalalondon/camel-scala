package uk.co.techneurons.messaging

import org.apache.camel.builder.RouteBuilder
import org.apache.camel.impl.DefaultCamelContext

/**
 * Send a message to rabbitmq.
 */
object RabbitMQProducer extends App {

  val camelContext = new DefaultCamelContext

  val rabbitMQEndpoint: String = "rabbitmq:localhost:5672/advert?autoAck=false&threadPoolSize=1&username=guest&password=guest&exchangeType=topic&autoDelete=false&declare=false"

  val rabbitMQRouteBuilder = new RouteBuilder() {
    override def configure(): Unit = {
      from("direct:start").to(rabbitMQEndpoint)
    }
  }

  camelContext.addRoutes(rabbitMQRouteBuilder)

  camelContext.start

  val producerTemplate = camelContext.createProducerTemplate

  producerTemplate.setDefaultEndpointUri("direct:start")

  producerTemplate.sendBodyAndHeader("{\"id\":1}","rabbitmq.ROUTING_KEY","advert.edited")
  producerTemplate.sendBodyAndHeader("{\"id\":2}","rabbitmq.ROUTING_KEY","advert.edited")

  camelContext.stop

}
