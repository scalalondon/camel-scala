package uk.co.techneurons.messaging

import org.slf4j.{LoggerFactory, Logger}

trait Logging {
  lazy val logger: Logger = LoggerFactory.getLogger(getClass.getName)
}
