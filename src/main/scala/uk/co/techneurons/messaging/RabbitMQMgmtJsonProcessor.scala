package uk.co.techneurons.messaging

import java.io.{InputStream, StringWriter}

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.apache.http.HttpEntity
import org.apache.http.auth.{UsernamePasswordCredentials, AuthScope}
import org.apache.http.client.CredentialsProvider
import org.apache.http.client.methods.{CloseableHttpResponse, HttpGet}
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.{HttpClients, CloseableHttpClient, BasicCredentialsProvider}
import java.nio.charset.StandardCharsets

import scala.collection.JavaConversions._

object RabbitMQMonitoring {

  val expectedRoutingKeys: Set[String] = Set("advert.archived.#",
      "advert.created.#",
      "advert.drafted.#",
      "advert.draftexpired.#",
      "advert.edited.other",
      "advert.expired.#",
      "advert.featured.#",
      "advert.featuredexpired.#",
      "advert.needsediting.#",
      "advert.published.#",
      "advert.removed.#",
      "advert.reposted.#",
      "advert.republished.#",
      "advert.updated.#")

  val mapper = new ObjectMapper()
  val hostname: String = "appmq.qa4.qa.gt.ecg.so"
  val port: Int = 15672
  val exchange = "advert.events"
  val queue = "finding.esapi.primary.advertsToProcess"
  val url: String = s"http://${hostname}:${port}/api/bindings/%2Fpublic/e/${exchange}/q/${queue}";
  val username = "gtops"
  val password = "123qwe"

  def rabbitMQHealthCheck(): Boolean = {

    def checkConnectivityAndConfiguration(json: String): Boolean = {
      val node = mapper.readTree(json)
      val routingKeys = node.elements().toList.map(n => n.path("routing_key").asText())
      routingKeys.size == 14 && routingKeys.toSet.equals(expectedRoutingKeys)
    }

    val rabbitMQResponse : Option[String] = rabbitMmgtRequest(hostname, port, username, password, url)
    checkConnectivityAndConfiguration(rabbitMQResponse.get)
  }


  def rabbitMmgtRequest(hostname: String, port: Int, username: String, password: String, url: String): Option[String] = {
    val httpclient: CloseableHttpClient = HttpClients.createDefault()
    val context: HttpClientContext = HttpClientContext.create()
    val credsProvider: CredentialsProvider = new BasicCredentialsProvider()
    credsProvider.setCredentials(new AuthScope(hostname, port), new UsernamePasswordCredentials(username, password))
    context.setCredentialsProvider(credsProvider)
    val closeableHttpResponse: CloseableHttpResponse = httpclient.execute(new HttpGet(url), context)
    var rabbitMQResponse: Option[String] = None
    try {
      val entity: HttpEntity = closeableHttpResponse.getEntity
      if (entity != null) {
        val is: InputStream = entity.getContent
        try {
          val sw = new StringWriter()
          IOUtils.copy(is, sw, StandardCharsets.UTF_8)
          rabbitMQResponse = Some(sw.toString())
        } finally {
          if(is != null) {
            is.close()
          }
        }
      }
    } finally {
      if(closeableHttpResponse != null) closeableHttpResponse.close()
      if(httpclient != null) httpclient.close()
    }
    rabbitMQResponse
  }

}
